<?php
    session_start();
    unset($_SESSION['pseudo']);
    unset($_SESSION['connexion']);
    header("Location: ../index.php");
    exit;  
?>
