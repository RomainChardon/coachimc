<?php 
    
try {
    $bdd = new PDO('mysql:host=localhost;dbname=coach imc', "root", "");
  } catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
  }

    $req = $bdd->query("SELECT AVG(poids / ((taille /100)*(taille /100))) AS'n_imc', regime, age FROM imc GROUP BY regime");
       ?>

<!-- Tableau Moyenne par régime -->
<link rel="stylesheet" href="./css/style.css">
       <div class="tableau">
         <table>
             <tr>
                 <th>
                     Régime
                 </th>
                 <th>
                     Moyenne IMC
                 </th>
             </tr>
             <?php while ($donnees = $req->fetch()) { ?> 
             <tr>
                 <td>
                    <?php echo $donnees['regime']; ?>
                 </td>
                 <td>
                    <?php echo $donnees['n_imc']; ?>
                 </td>
             </tr>
             <?php } ?>
         </table>  
       </div>
<!-- Tableau Moyenne par age -->
        <?php
        $req2 = $bdd->query("SELECT poids / ((taille /100)*(taille /100)) AS'imc', age FROM imc order BY age");
        $compteur20 = 0;
        $total_20 = 0;
        $compteur40 = 0;
        $total_40 = 0;
        $compteur60 = 0;
        $total_60 = 0;
        $compteur80 = 0;
        $total_80 = 0;
        while($donnees2 = $req2->fetch()){
            if($donnees2['age'] <= 20){
                $compteur20 = $compteur20 + 1;
                $total_20 = $total_20 + $donnees2['imc'];
            } else if (($donnees2['age'] <= 40)&&($donnees2['age'] >= 21)){
                $compteur40 = $compteur40 + 1;
                $total_40 = $total_40 + $donnees2['imc'];
            } else if (($donnees2['age'] <= 60)&&($donnees2['age'] >= 41)){
                $compteur60 = $compteur60 + 1;
                $total_60 = $total_60 + $donnees2['imc'];
            } else if ($donnees2['age'] >= 61){
                $compteur80 = $compteur80 + 1;
                $total_80 = $total_80 + $donnees2['imc'];
            }
        }
       ?>
       <div class="tableau">
         <table>
             <tr>
                 <th>
                     Age
                 </th>
                 <th>
                     Moyenne IMC
                 </th>
             </tr>
             <tr>
                 <td>
                     0-20
                 </td>
                 <td>
                    <?php 
                        $M_total20 = $total_20 / $compteur20;
                        echo $M_total20;
                    ?>
                 </td>
             </tr>
             <tr>
                 <td>
                     21-40
                 </td>
                 <td>
                    <?php 
                        $M_total40 = $total_40 / $compteur40;
                        echo $M_total40;
                    ?>
                 </td>
             </tr>
             <tr>
                 <td>
                     41-60
                 </td>
                 <td>
                    <?php 
                        $M_total60 = $total_60 / $compteur60;
                        echo $M_total60;
                    ?>
                 </td>
             </tr>
             <tr>
                 <td>
                     61 - +
                 </td>
                 <td>
                    <?php 
                        $M_total80 = $total_80 / $compteur80;
                        echo $M_total80;
                    ?>
                 </td>
             </tr>
         </table>  
       </div>
