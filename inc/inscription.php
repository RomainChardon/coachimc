<?php 

try 
{
  $bdd = new PDO('mysql:host=localhost;dbname=coach imc', "root", "");
} 
catch (PDOException $e) 
{
  print "Erreur !: " . $e->getMessage() . "<br/>";
  die();
}


// INSCRIPTION 
  if(isset($_POST['form_inscription']))
  {
      if(!empty($_POST['pseudo']) AND !empty($_POST['password']) AND !empty($_POST['conf_password']))
      {
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $password = $_POST['password'];
        $conf_password = htmlspecialchars($_POST['conf_password']);

        $pseudolength = strlen($pseudo);
        if($pseudolength <= 25)
        { 
          $reqpseudo = $bdd->prepare("SELECT * FROM login WHERE pseudo = ?");
          $reqpseudo->execute(array($pseudo));
          $pseudoexist = $reqpseudo->rowCount();
          if($pseudoexist == 0)
          {
            if($password == $conf_password)
            {
              $passwordlenght = strlen($password);
              if($passwordlenght >= 4)
              {
                $hashpassword = password_hash($password, PASSWORD_DEFAULT);
                $inscri = $bdd->prepare("INSERT INTO login(pseudo, password) VALUES(?,?)");
                $inscri->execute(array($pseudo, $hashpassword));
                $erreur = "Votre compte est créer !";
                $_SESSION['pseudo'] = $pseudo;
                $delai=0; 
                header("Refresh: $delai;");
                exit();
              }
              else
              {
                $erreur = "Mot de passe ne doit pas être inférieur a 4 caractère.";
              }
            }
            else
            {
              $erreur = "Les mot de passe ne sont pas identique.";
            }
          }
          else
          {
            $erreur = "Pseudo déja utiliser.";
          }
        }
        else
        {
          $erreur = "Votre pseudo ne doit pas dépacer 25 caractères.";
        }
      } 
      else
      {
        $erreur = "Remplire tous les champs.";
      }
      
  }

?>
<div>
    <h2>Inscription</h2>
    <form method="POST" action="">
      <input type="text" placeholder="Votre pseudo :" name="pseudo" >
      <input type="password" placeholder="Votre mot de passe :" name="password" >
      <input type="password" placeholder="Confimez mot de passe :" name="conf_password" >
      <input type="submit" value="Inscription" name="form_inscription">
    </form>
</div>
    <?php // Affichage des erreurs
        if(isset($erreur))
        {
            echo $erreur;
        }

    ?>
