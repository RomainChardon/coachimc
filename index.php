<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>CoachIMC</title>
</head>
<body>
  <header align="center">
    <h1>CoachIMC</h1>
    <p>Inscrivez-vous ou connectez-vous pour participer au sondage !
      <br> Vous pourrez accèder au résultat directement sur la page de membre.
    </p>
  </header>

  
  <div class="contener" align="center">
    <?php
      include_once './inc/imc_form.php';
    ?>
    
  </div>

  <div class="contener" align="center">
    <?php
    if (isset($_SESSION['pseudo'])){
      ?> <h1> Bonjour <?php echo $_SESSION['pseudo']; ?> !  </h1>
      <?php include_once './inc/resultats.php'; ?>
      <a href="./inc/deco.php">Se déconnecter</a><?php
    } else {
      include_once './inc/acceuil.html';
      include_once './inc/router.php';
    }
    ?> 
  </div>

</body>
</html>